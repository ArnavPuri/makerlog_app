import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:get_makerlog/ui/main_page.dart';
import 'package:get_makerlog/ui/spash_screen.dart';

void main() async {
  runApp(new MyApp());
}

var url = "https://api.getmakerlog.com/api-token-auth/";

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Makerlog',
      theme: new ThemeData(
        fontFamily: 'Poppins',
        primarySwatch: Colors.green,
      ),
      home: new SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreen3State createState() => new _LoginScreen3State();
}

class _LoginScreen3State extends State<LoginScreen>
    with TickerProviderStateMixin {
  final TextEditingController _userNameFilter = new TextEditingController();
  final TextEditingController _passwordFilter = new TextEditingController();
  String _username = "";
  String _password = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  //input field params
  final FocusNode _passFocus = FocusNode();
  final FocusNode _usernameFocus = FocusNode();

  void _userNameListen() {
    if (_userNameFilter.text.isEmpty) {
      _username = "";
    } else {
      _username = _userNameFilter.text;
    }
  }

  void _passwordListen() {
    if (_passwordFilter.text.isEmpty) {
      _password = "";
    } else {
      _password = _passwordFilter.text;
    }
  }

  _LoginScreen3State() {
    _userNameFilter.addListener(_userNameListen);
    _passwordFilter.addListener(_passwordListen);
  }

  @override
  void initState() {
    super.initState();
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }

    await Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SplashScreen()),
    );
  }

  Widget HomePage() {
    return new Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Colors.black,
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.2), BlendMode.dstATop),
          image: AssetImage('assets/images/mountains.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: new Column(
        children: <Widget>[
          Container(
            child: Image.asset("assets/images/logo.png"),
            padding: EdgeInsets.fromLTRB(120.0, 160.0, 120.0, 10.0),
          ),
          Container(
            padding: EdgeInsets.only(top: 20.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Makerlog",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
          ),
//          new Container(
//            width: MediaQuery.of(context).size.width,
//            margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 150.0),
//            alignment: Alignment.center,
//            child: new Row(
//              children: <Widget>[
//                new Expanded(
//                  child: new OutlineButton(
//                    shape: new RoundedRectangleBorder(
//                        borderRadius: new BorderRadius.circular(30.0)),
//                    color: Colors.greenAccent,
//                    highlightedBorderColor: Colors.white,
//                    onPressed: () => gotoSignup(),
//                    child: new Container(
//                      padding: const EdgeInsets.symmetric(
//                        vertical: 20.0,
//                        horizontal: 20.0,
//                      ),
//                      child: new Row(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          new Expanded(
//                            child: Text(
//                              "SIGN UP",
//                              textAlign: TextAlign.center,
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontWeight: FontWeight.bold),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//          ),
          new Container(
            width: MediaQuery.of(context).size.width,
            margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 130.0),
            alignment: Alignment.bottomCenter,
            child: new Row(
              children: <Widget>[
                new Expanded(
                  child: new FlatButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Colors.white,
                    onPressed: () => gotoLogin(),
                    child: new Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 20.0,
                        horizontal: 20.0,
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Expanded(
                            child: Text(
                              "LOGIN",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget LoginPage() {
    return new Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: true,
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.05), BlendMode.dstATop),
            image: AssetImage('assets/images/mountains.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: new Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(120.0, 90.0, 120.0, 30),
              child: Center(child: Image.asset("assets/images/logo.png")),
            ),
            new Container(
              padding: EdgeInsets.all(12.0),
              margin: EdgeInsets.only(left: 12.0, right: 12.0),
              child: TextFormField(
                controller: _userNameFilter,
                autofocus: false,
                focusNode: _usernameFocus,
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (term) {
                  _usernameFocus.unfocus();
                  FocusScope.of(context).requestFocus(_passFocus);
                },
                decoration: InputDecoration(
                  hintText: 'elonmusk',
                  labelText: 'Username',
                  hintStyle: TextStyle(color: Colors.black26),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                ),
              ),
            ),
            new Container(
              padding: EdgeInsets.all(12.0),
              margin: EdgeInsets.only(left: 12.0, right: 12.0),
              child: TextFormField(
                controller: _passwordFilter,
                focusNode: _passFocus,
                autofocus: false,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: '****',
                  labelText: 'Password',
                  hintStyle: TextStyle(color: Colors.black26),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                ),
              ),
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 20.0),
              alignment: Alignment.center,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new FlatButton(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      color: Colors.greenAccent,
                      child: new Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 20.0,
                          horizontal: 20.0,
                        ),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Expanded(
                              child: Text(
                                "LOGIN",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onPressed: () {
                        _getCredentials(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

//  Widget SignupPage() {
//    return new Scaffold(
//      body: Container(
//        height: MediaQuery.of(context).size.height,
//        decoration: BoxDecoration(
//          color: Colors.white,
//          image: DecorationImage(
//            colorFilter: new ColorFilter.mode(
//                Colors.black.withOpacity(0.05), BlendMode.dstATop),
//            image: AssetImage('assets/images/mountains.jpg'),
//            fit: BoxFit.cover,
//          ),
//        ),
//        child: new Column(
//          children: <Widget>[
//            Container(
//              padding: EdgeInsets.all(100.0),
//              child: Center(child: Image.asset("assets/images/logo.png")),
//            ),
//            new Row(
//              children: <Widget>[
//                new Expanded(
//                  child: new Padding(
//                    padding: const EdgeInsets.only(left: 40.0),
//                    child: new Text(
//                      "EMAIL",
//                      style: TextStyle(
//                        fontWeight: FontWeight.bold,
//                        color: Color.fromARGB(1, 71, 224, 160),
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//            new Container(
//              width: MediaQuery.of(context).size.width,
//              margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
//              alignment: Alignment.center,
//              decoration: BoxDecoration(
//                border: Border(
//                  bottom: BorderSide(
//                      color: Color.fromARGB(1, 71, 224, 160),
//                      width: 0.5,
//                      style: BorderStyle.solid),
//                ),
//              ),
//              padding: const EdgeInsets.only(left: 0.0, right: 10.0),
//              child: new Row(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  new Expanded(
//                    child: TextField(
//                      obscureText: true,
//                      textAlign: TextAlign.left,
//                      decoration: InputDecoration(
//                        border: InputBorder.none,
//                        hintText: 'samarthagarwal@live.com',
//                        hintStyle: TextStyle(color: Colors.grey),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Divider(
//              height: 24.0,
//            ),
//            new Row(
//              children: <Widget>[
//                new Expanded(
//                  child: new Padding(
//                    padding: const EdgeInsets.only(left: 40.0),
//                    child: new Text(
//                      "PASSWORD",
//                      style: TextStyle(
//                        fontWeight: FontWeight.bold,
//                        color: Color.fromARGB(1, 71, 224, 160),
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//            new Container(
//              width: MediaQuery.of(context).size.width,
//              margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
//              alignment: Alignment.center,
//              decoration: BoxDecoration(
//                border: Border(
//                  bottom: BorderSide(
//                      color: Colors.greenAccent,
//                      width: 0.5,
//                      style: BorderStyle.solid),
//                ),
//              ),
//              padding: const EdgeInsets.only(left: 0.0, right: 10.0),
//              child: new Row(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  new Expanded(
//                    child: TextField(
//                      obscureText: true,
//                      textAlign: TextAlign.left,
//                      decoration: InputDecoration(
//                        border: InputBorder.none,
//                        hintText: '*********',
//                        hintStyle: TextStyle(color: Colors.grey),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Divider(
//              height: 24.0,
//            ),
//            new Row(
//              children: <Widget>[
//                new Expanded(
//                  child: new Padding(
//                    padding: const EdgeInsets.only(left: 40.0),
//                    child: new Text(
//                      "CONFIRM PASSWORD",
//                      style: TextStyle(
//                        fontWeight: FontWeight.bold,
//                        color: Colors.greenAccent,
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//            new Container(
//              width: MediaQuery.of(context).size.width,
//              margin: const EdgeInsets.only(left: 40.0, right: 40.0, top: 10.0),
//              alignment: Alignment.center,
//              decoration: BoxDecoration(
//                border: Border(
//                  bottom: BorderSide(
//                      color: Colors.greenAccent,
//                      width: 0.5,
//                      style: BorderStyle.solid),
//                ),
//              ),
//              padding: const EdgeInsets.only(left: 0.0, right: 10.0),
//              child: new Row(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.start,
//                children: <Widget>[
//                  new Expanded(
//                    child: TextField(
//                      obscureText: true,
//                      textAlign: TextAlign.left,
//                      decoration: InputDecoration(
//                        border: InputBorder.none,
//                        hintText: '*********',
//                        hintStyle: TextStyle(color: Colors.grey),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//            Divider(
//              height: 24.0,
//            ),
//            new Row(
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                Padding(
//                  padding: const EdgeInsets.only(right: 20.0),
//                  child: new FlatButton(
//                    child: new Text(
//                      "Already have an account?",
//                      style: TextStyle(
//                        fontWeight: FontWeight.bold,
//                        color: Colors.greenAccent,
//                        fontSize: 15.0,
//                      ),
//                      textAlign: TextAlign.end,
//                    ),
//                    onPressed: () => {},
//                  ),
//                ),
//              ],
//            ),
//            new Container(
//              width: MediaQuery.of(context).size.width,
//              margin: const EdgeInsets.only(left: 30.0, right: 30.0, top: 50.0),
//              alignment: Alignment.center,
//              child: new Row(
//                children: <Widget>[
//                  new Expanded(
//                    child: new FlatButton(
//                      shape: new RoundedRectangleBorder(
//                        borderRadius: new BorderRadius.circular(30.0),
//                      ),
//                      color: Colors.greenAccent,
//                      onPressed: () => {},
//                      child: new Container(
//                        padding: const EdgeInsets.symmetric(
//                          vertical: 20.0,
//                          horizontal: 20.0,
//                        ),
//                        child: new Row(
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          children: <Widget>[
//                            new Expanded(
//                              child: Text(
//                                "SIGN UP",
//                                textAlign: TextAlign.center,
//                                style: TextStyle(
//                                    color: Colors.white,
//                                    fontWeight: FontWeight.bold),
//                              ),
//                            ),
//                          ],
//                        ),
//                      ),
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }

  gotoLogin() {
    //controller_0To1.forward(from: 0.0);
    _controller.animateToPage(
      2,
      duration: Duration(milliseconds: 800),
      curve: Curves.bounceOut,
    );
  }

//
//  gotoSignup() {
//    //controller_minus1To0.reverse(from: 0.0);
//    _controller.animateToPage(
//      0,
//      duration: Duration(milliseconds: 800),
//      curve: Curves.bounceOut,
//    );
//  }

  PageController _controller =
      new PageController(initialPage: 0, viewportFraction: 1.0);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        child: PageView(
          controller: _controller,
          physics: new AlwaysScrollableScrollPhysics(),
          children: <Widget>[HomePage(), LoginPage()],
          scrollDirection: Axis.horizontal,
        ));
  }

  _getCredentials(BuildContext c) async {
//    await _showDailyAtTime();
    http.post(url, body: {'username': _username, 'password': _password}).then(
        (response) {
      final responseJson = json.decode(response.body);
      if (responseJson['token'] != null) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainPage()));

        SharedPreferences.getInstance().then((value) {
          value.setString("token", responseJson["token"]);
        });
      } else {
        _displaySnackBar();
      }
    });
  }

  _displaySnackBar() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Wrong credentials, try again"),
      action: SnackBarAction(
          label: "Reset",
          onPressed: () {
            _userNameFilter.text = "";
            _passwordFilter.text = "";
          }),
    ));
  }
}
