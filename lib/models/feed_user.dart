import 'package:get_makerlog/models/task.dart';
import 'package:get_makerlog/models/ml_user.dart';

class FeedUser {
  User user;
  List<Task> tasks;

  FeedUser(this.user, this.tasks);
}
