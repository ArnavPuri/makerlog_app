class User {
  int id;
  String firstName;
  String name;
  String username;
  String avatar;
  int streak;
  int tda;
  String twitter;
  String instagram;
  String github;
  String bio;

  User(
      this.id,
      this.name,
      this.firstName,
      this.username,
      this.avatar,
      this.streak,
      this.tda,
      this.twitter,
      this.instagram,
      this.github,
      this.bio);

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        json["id"],
        "${json["first_name"]} ${json["last_name"]}",
        json["first_name"],
        json["username"],
        json["avatar"],
        json["streak"],
        json["week_tda"],
        json["twitter_handle"],
        json["instagram_handle"],
        json["github_handle"],
        json["description"]);
  }
}
