class Task {
  int id;
  String task;
  int praise;
  bool done;
  bool in_progress;
  String attachment;
  String dateAdded;
  String dateCompleted;
  Task(this.id, this.task, this.praise, this.done, this.in_progress,
      this.attachment, this.dateAdded, this.dateCompleted);

//  mapping to json data
  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
        json["id"],
        json["content"],
        json["praise"],
        json["done"],
        json["in_progress"],
        json["attachment"],
        json["created_at"],
        json["done_at"]);
  }
}
