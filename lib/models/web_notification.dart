class WebNotification {
  bool read;
  String key;
  String verb;
  String actor;
  String target;

  WebNotification(this.read, this.key, this.verb, this.target, this.actor);

  factory WebNotification.fromJson(Map<String, dynamic> json) {
    return WebNotification(json["read"], json["key"], json["verb"],
        json["target"]["content"], json["actor"]["first_name"]);
  }
}
