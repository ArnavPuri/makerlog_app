import 'dart:math';

import 'package:get_makerlog/ui/SwipeAnimation/detail.dart';
import 'package:flutter/material.dart';
import 'package:get_makerlog/models/ml_user.dart';

//TODO remove original implementation of cd
//Positioned cardDemo(
//    DecorationImage img,
//    double bottom,
//    double right,
//    double left,
//    double cardWidth,
//    double rotation,
//    double skew,
//    BuildContext context,
//    Function dismissImg,
//    int flag,
//    Function addImg,
//    Function swipeRight,
//    Function swipeLeft)
Positioned cardDemo(
    User user,
    double bottom,
    double right,
    double left,
    double cardWidth,
    double rotation,
    double skew,
    BuildContext context,
    Function dissmissUser,
    int flag,
    Function addUser,
    Function swipeRight,
    Function swipeLeft) {
  Size screenSize = MediaQuery.of(context).size;
  // print("Card");
  print(user.avatar);
  return new Positioned(
    bottom: 100.0 + bottom,
    right: flag == 0 ? right != 0.0 ? right : null : null,
    left: flag == 1 ? right != 0.0 ? right : null : null,
    child: Padding(
      padding: EdgeInsets.all(12.0),
      child: new Dismissible(
        key: new Key(new Random().toString()),
//      crossAxisEndOffset: -0.3,
        onResize: () {
          //print("here");
          // setState(() {
          //   var i = data.removeLast();

          //   data.insert(0, i);
          // });
        },
        onDismissed: (DismissDirection direction) {
//          _swipeAnimation();
          if (direction == DismissDirection.endToStart)
            dissmissUser(user);
          else
            addUser(user);
        },
        child: new Transform(
          alignment: flag == 0 ? Alignment.bottomRight : Alignment.bottomLeft,
          //transform: null,
          transform: new Matrix4.skewX(skew),
          //..rotateX(-math.pi / rotation),
          child: new RotationTransition(
            turns: new AlwaysStoppedAnimation(
                flag == 0 ? rotation / 360 : -rotation / 360),
            child: new Hero(
              tag: user.username,
              child: new GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => new DetailPage(type: img)));
                  Navigator.of(context).push(new PageRouteBuilder(
                    pageBuilder: (_, __, ___) => new DetailPage(user: user),
                  ));
                },
                child: new Card(
                  color: Colors.transparent,
                  elevation: 4.0,
                  child: new Container(
                    alignment: Alignment.center,
                    width: screenSize.width - 50,
                    height: screenSize.height,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    child: new Column(
                      children: <Widget>[
                        new Container(
                          decoration: new BoxDecoration(
                            borderRadius: new BorderRadius.only(
                                topLeft: new Radius.circular(8.0),
                                topRight: new Radius.circular(8.0)),
                            image: new DecorationImage(
                                fit: BoxFit.fitWidth,
                                image: user.avatar != null
                                    ? new NetworkImage(user.avatar)
                                    : new AssetImage(
                                        "assets/images/placeholder.png")),
                          ),
                          width: 250,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
