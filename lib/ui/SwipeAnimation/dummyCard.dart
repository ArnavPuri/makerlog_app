import 'package:flutter/material.dart';
import 'package:get_makerlog/models/ml_user.dart';

Positioned cardDemoDummy(User user, double bottom, double right, double left,
    double cardWidth, double rotation, double skew, BuildContext context) {
  Size screenSize = MediaQuery.of(context).size;
  // Size screenSize=(500.0,200.0);
  // print("dummyCard");
  return new Positioned(
    bottom: 100.0 + bottom,
    // right: flag == 0 ? right != 0.0 ? right : null : null,
    //left: flag == 1 ? right != 0.0 ? right : null : null,
    child: new Card(
      color: Colors.transparent,
      elevation: 4.0,
      child: new Container(
        alignment: Alignment.center,
        width: screenSize.width - 50,
        height: screenSize.height,
        decoration: new BoxDecoration(
          color: Colors.greenAccent,
          borderRadius: new BorderRadius.circular(8.0),
        ),
        child: new Column(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.only(
                    topLeft: new Radius.circular(8.0),
                    topRight: new Radius.circular(8.0)),
                image: new DecorationImage(
                    image: user.avatar != null
                        ? new NetworkImage(
                            user.avatar,
                          )
                        : new AssetImage("assets/images/placeholder.png"),
                    fit: BoxFit.contain),
              ),
              width: 250,
            ),
          ],
        ),
      ),
    ),
  );
}
