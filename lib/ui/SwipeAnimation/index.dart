import 'dart:async';
import 'package:get_makerlog/models/feed_user.dart';
import 'package:get_makerlog/models/task.dart';
import 'package:get_makerlog/models/ml_user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/scheduler.dart' show timeDilation;
import 'dart:io';
import 'dart:convert';
import 'dart:math';
import 'package:url_launcher/url_launcher.dart';

class CardDemo extends StatefulWidget {
  @override
  CardDemoState createState() => new CardDemoState();
}

class CardDemoState extends State<CardDemo> with TickerProviderStateMixin {
  AnimationController _buttonController;
  Animation<double> rotate;
  Animation<double> right;
  Animation<double> bottom;
  Animation<double> width;
  int flag = 0;

//  List data = imageData;
  List selectedData = [];

//  real data vars
  var fetchLatestStreamURL = "https://api.getmakerlog.com/explore/stream/";

  var streamDataUrl = "";
  Future<List<User>> usersList;
  Future<List<FeedUser>> feedUsersList;
  List<User> userData = [];
  List<FeedUser> feedUserData = [];
  List<dynamic> allTasks = [];
  List<FeedUser> userCards = [];

  Future<List<FeedUser>> getStream(String streamUrl) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response = await http.get(
      streamUrl,
      headers: {HttpHeaders.authorizationHeader: "Token " + token},
    ).then((response) {
      allTasks = json.decode(utf8.decode(response.bodyBytes))["data"];
      var userList =
          (json.decode(utf8.decode(response.bodyBytes))["data"] as List)
              .map((content) => new User.fromJson(content["user"]))
              .toList();

      uniqueItemsList(userList);
//
//      var temp = userList.map((user) => user.id).toSet().toList();
//      print("sure unique ${temp.length}");
//      print("unique ids $temp");
//      print("not sure unique ${userList.length}");
      userList.forEach((user) {
        List<Task> userTasks = [];
        allTasks.forEach((task) {
          if (task["user"]["id"] == user.id) {
            userTasks.add(new Task.fromJson(task));
          }
        });
        feedUserData.add(new FeedUser(user, userTasks));
      });
      return feedUserData.reversed.toList();
//      return userList;
    });
    return response;
  }

  Future<String> getLatestStream() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');

    final response = await http.get(
      fetchLatestStreamURL,
      headers: {HttpHeaders.authorizationHeader: "Token " + token},
    ).then((response) {
      return json.decode(response.body)["latest_url"];
    });
    setState(() {
      streamDataUrl = response;
    });
    return response;
  }

  void initState() {
    super.initState();

    _buttonController = new AnimationController(
        duration: new Duration(milliseconds: 1000), vsync: this);

    rotate = new Tween<double>(
      begin: -0.0,
      end: -40.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    rotate.addListener(() {
      setState(() {
        if (rotate.isCompleted) {
          var i = userCards.removeLast();
          userCards.insert(0, i);
          var j = feedUserData.removeLast();
          feedUserData.insert(0, j);

          _buttonController.reset();
        }
      });
    });

    right = new Tween<double>(
      begin: 0.0,
      end: 400.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    bottom = new Tween<double>(
      begin: 15.0,
      end: 100.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.ease,
      ),
    );
    width = new Tween<double>(
      begin: 20.0,
      end: 25.0,
    ).animate(
      new CurvedAnimation(
        parent: _buttonController,
        curve: Curves.bounceOut,
      ),
    );
    getLatestStream().then((status) {
      feedUsersList = getStream(streamDataUrl);
      feedUsersList.then((list) {
        feedUserData = list;

//        userData.forEach((user) {
//          List<Task> userTasks = [];
//          allTasks.forEach((task) {
//            if (task["user"]["id"] == user.id) {
//              userTasks.add(new Task.fromJson(task));
//            }
//          });
//          feedUserData.add(new FeedUser(user, userTasks));
//        });
      });
    });
  }

  @override
  void dispose() {
    _buttonController.dispose();
    super.dispose();
  }

  Future<Null> _swipeAnimation() async {
    try {
      await _buttonController.forward();
    } on TickerCanceled {}
  }
//
//  dismissImg(DecorationImage img) {
//    setState(() {
//      data.remove(img);
//    });
//  }

  dismissUser(FeedUser fs) {
    setState(() {
      feedUserData.remove(fs);
      userCards.remove(fs);
//      userData.remove(user);
    });
  }

//  addImg(DecorationImage img) {
//    setState(() {
//      data.remove(img);
//      selectedData.add(img);
//    });
//  }

  addUser(FeedUser fs) {
    setState(() {
      selectedData.add(fs);
      feedUserData.remove(fs);
      userCards.remove(fs);
//      userData.add(user);
//      selectedData.add(img);
    });
  }

  swipeRight() {
    if (flag == 0)
      setState(() {
        flag = 1;
      });
    _swipeAnimation();
  }

  swipeLeft() {
    if (flag == 1)
      setState(() {
        flag = 0;
      });
    _swipeAnimation();
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 0.3;

    double initialBottom = 10;
//    var dataLength = data.length;

    double backCardWidth = -10.0;
    return new FutureBuilder<List<FeedUser>>(
      future: feedUsersList,
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          var totalUsers = feedUserData.length;

          if (userCards.length == 0) {
            if (totalUsers > 9) {
              userCards = feedUserData.sublist(feedUserData.length - 9);
            } else {
              userCards = feedUserData;
            }
          }

          double backCardPosition =
              initialBottom + (userCards.length - 1) * 10 + 10;
          return Container(
              alignment: AlignmentDirectional.center,
              child: Padding(
                  padding: EdgeInsets.fromLTRB(1.0, 0, 1.0, 0),
                  child: totalUsers > 0
                      ? new Stack(
                          alignment: AlignmentDirectional.center,
                          children: userCards.map((item) {
                            if (userCards.indexOf(item) ==
                                userCards.length - 1) {
                              return cardDemo(
                                  item,
                                  item.user,
                                  bottom.value,
                                  right.value,
                                  0.0,
                                  backCardWidth + 10,
                                  rotate.value,
                                  rotate.value < -10 ? 0.1 : 0.0,
                                  context,
                                  dismissUser,
                                  flag,
                                  addUser,
                                  swipeRight,
                                  swipeLeft,
                                  item.tasks);
                            } else {
                              backCardPosition = backCardPosition - 10;
                              backCardWidth = backCardWidth + 10;

                              return cardDemoDummy(
                                  item.user,
                                  backCardPosition,
                                  0.0,
                                  0.0,
                                  backCardWidth,
                                  0.0,
                                  0.0,
                                  context,
                                  item.tasks);
                            }
                          }).toList(),
                        )
                      : Center(
                          child: new Text("That's all happened today ✌️",
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  color: Colors.white, fontSize: 30.0)),
                        )));
        } else {
          return new Center(child: new CircularProgressIndicator());
        }
      },
    );
  }

  void uniqueItemsList(List<User> list) {
    for (int i = 0; i < list.length; i++) {
      User user = list[i];
      int index = -1;
      // Remove duplicates
      do {
        index = myIndexOf(list, user, i);
        if (index != -1) {
          list.removeAt(index);
        }
      } while (index != -1);
    }
  }

  int myIndexOf(arr, o, currentIndex) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].id == o.id) {
        if (i != currentIndex) return i;
      }
    }
    return -1;
  }

//  Stack cards ui
  //active card
  Positioned cardDemo(
      FeedUser fs,
      User user,
      double bottom,
      double right,
      double left,
      double cardWidth,
      double rotation,
      double skew,
      BuildContext context,
      Function dissmissUser,
      int flag,
      Function addUser,
      Function swipeRight,
      Function swipeLeft,
      List<Task> tasks) {
    Size screenSize = MediaQuery.of(context).size;
    // print("Card");

    return new Positioned(
      bottom: 20.0 + bottom,
      right: flag == 0 ? right != 0.0 ? right : null : null,
      left: flag == 1 ? right != 0.0 ? right : null : null,
      child: new Dismissible(
        key: Key(new Random().toString()),
        crossAxisEndOffset: -0.3,
        onResize: () {
          //print("here");
          // setState(() {
          //   var i = data.removeLast();

          //   data.insert(0, i);
          // });
        },
        onDismissed: (DismissDirection direction) {
//          _swipeAnimation();

          if (direction == DismissDirection.endToStart)
            dissmissUser(fs);
          else
            addUser(fs);
        },
        child: new Transform(
          alignment: flag == 0 ? Alignment.bottomRight : Alignment.bottomLeft,
          //transform: null,
          transform: new Matrix4.skewX(skew),
          //..rotateX(-math.pi / rotation),
          child: new RotationTransition(
            turns: new AlwaysStoppedAnimation(
                flag == 0 ? rotation / 360 : -rotation / 360),
            child: new Hero(
              tag: user.username,
              child: new GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     new MaterialPageRoute(
                  //         builder: (context) => new DetailPage(type: img)));
//                  Navigator.of(context).push(new PageRouteBuilder(
//                    pageBuilder: (_, __, ___) => new DetailPage(user: user),
//                  ));
                },
                child: new Card(
                  color: Colors.transparent,
                  elevation: 6.0,
                  child: new Container(
                      alignment: Alignment.center,
                      width: screenSize.width - 40,
                      height: screenSize.height - 240,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(8.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                      fit: BoxFit.contain,
                                      image: user.avatar != null
                                          ? new NetworkImage(user.avatar)
                                          : new AssetImage(
                                              "assets/images/placeholder.png")),
                                  shape: BoxShape.circle,
                                  color: Colors.greenAccent),
                              width: 120,
                              height: 120,
                            ),
//                            general info
                            new Container(
                              margin: EdgeInsets.fromLTRB(6, 6, 6, 6),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Text(
                                    "🔥${user.streak}",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  new Text(
                                    user.name,
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  new Text(
                                    "🏁${user.tda}",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                ],
                              ),
                            ),
//                            social media row
                            new Container(
                              margin: EdgeInsets.fromLTRB(6, 6, 6, 6),
                              child: new Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  user.github != null
                                      ? new InkWell(
                                          child: new Image.asset(
                                            "assets/images/github.png",
                                            width: 50,
                                            height: 50,
                                          ),
                                          onTap: () {
                                            _launchURL(
                                                "https://www.github.com/${user.github}");
                                          },
                                        )
                                      : new Container(),
                                  user.twitter != null
                                      ? new InkWell(
                                          child: new Image.asset(
                                            "assets/images/twitter.png",
                                            width: 50,
                                            height: 50,
                                          ),
                                          onTap: () {
                                            _launchURL(
                                                "https://www.twitter.com/${user.twitter}");
                                          },
                                        )
                                      : new Container(),
                                  user.instagram != null
                                      ? new InkWell(
                                          child: new Image.asset(
                                            "assets/images/instagram.png",
                                            width: 50,
                                            height: 50,
                                          ),
                                          onTap: () {
                                            _launchURL(
                                                "https://www.instagram.com/${user.instagram}");
                                          },
                                        )
                                      : new Container()
                                ],
                              ),
                            ),
//                            tasks list
                            new Container(
                                alignment: AlignmentDirectional.centerStart,
                                padding: EdgeInsets.only(left: 8.0),
                                child: new Text("Working on:")),
                            new Expanded(
                                child: ListView.builder(
                                    itemCount: tasks.length,
                                    padding: EdgeInsets.all(6.0),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return new Card(
                                        elevation: 1.0,
                                        color: Color.fromRGBO(58, 66, 86, 1.0),
                                        child: ListTile(
                                          title: Text(
                                            tasks[index].task,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12.0),
                                          ),
                                          leading: tasks[index].done
                                              ? Icon(
                                                  Icons.done_all,
                                                  color: Colors.greenAccent,
                                                )
                                              : Icon(
                                                  Icons.watch_later,
                                                  color: Colors.yellow,
                                                ),
                                        ),
                                      );
                                    }))
                          ],
                        ),
                      )),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Positioned cardDemoDummy(
      User user,
      double bottom,
      double right,
      double left,
      double cardWidth,
      double rotation,
      double skew,
      BuildContext context,
      List<Task> tasks) {
    Size screenSize = MediaQuery.of(context).size;
    // Size screenSize=(500.0,200.0);
    // print("dummyCard");
    return new Positioned(
      bottom: 30 + bottom,
      // right: flag == 0 ? right != 0.0 ? right : null : null,
      //left: flag == 1 ? right != 0.0 ? right : null : null,
      child: new Card(
        color: Colors.transparent,
        elevation: 4.0,
        child: new Container(
            alignment: Alignment.center,
            width: screenSize.width - 35,
            height: screenSize.height - 240,
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.circular(8.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: new Column(
                children: <Widget>[
                  new Container(
                    decoration: new BoxDecoration(
                        image: new DecorationImage(
                            fit: BoxFit.contain,
                            image: user.avatar != null
                                ? new NetworkImage(user.avatar)
                                : new AssetImage(
                                    "assets/images/placeholder.png")),
                        shape: BoxShape.circle,
                        color: Colors.greenAccent),
                    width: 120,
                    height: 120,
                  ),
//                            general info
                  new Container(
                    margin: EdgeInsets.fromLTRB(6, 6, 6, 6),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "🔥${user.streak}",
                          style: TextStyle(fontSize: 18.0),
                        ),
                        new Text(
                          user.name,
                          style: TextStyle(fontSize: 18.0),
                        ),
                        new Text(
                          "🏁${user.tda}",
                          style: TextStyle(fontSize: 18.0),
                        ),
                      ],
                    ),
                  ),
//                            social media row
                  new Container(
                    margin: EdgeInsets.fromLTRB(6, 6, 6, 6),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        user.github != null
                            ? new InkWell(
                                child: new Image.asset(
                                  "assets/images/github.png",
                                  width: 50,
                                  height: 50,
                                ),
                                onTap: () {
                                  _launchURL(
                                      "https://www.github.com/${user.github}");
                                },
                              )
                            : new Container(),
                        user.twitter != null
                            ? new InkWell(
                                child: new Image.asset(
                                  "assets/images/twitter.png",
                                  width: 50,
                                  height: 50,
                                ),
                                onTap: () {
                                  _launchURL(
                                      "https://www.twitter.com/${user.twitter}");
                                },
                              )
                            : new Container(),
                        user.instagram != null
                            ? new InkWell(
                                child: new Image.asset(
                                  "assets/images/instagram.png",
                                  width: 50,
                                  height: 50,
                                ),
                                onTap: () {
                                  _launchURL(
                                      "https://www.instagram.com/${user.instagram}");
                                },
                              )
                            : new Container()
                      ],
                    ),
                  ),
                  new Container(
                      alignment: AlignmentDirectional.centerStart,
                      padding: EdgeInsets.only(left: 8.0),
                      child: new Text("Working on:")),
                  new Expanded(
                      child: ListView.builder(
                          itemCount: tasks.length,
                          padding: EdgeInsets.all(6.0),
                          itemBuilder: (BuildContext context, int index) {
                            return new Card(
                              elevation: 1.0,
                              color: Color.fromRGBO(58, 66, 86, 1.0),
                              child: ListTile(
                                title: Text(
                                  tasks[index].task,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12.0),
                                ),
                                leading: tasks[index].done
                                    ? Icon(
                                        Icons.done_all,
                                        color: Colors.greenAccent,
                                      )
                                    : Icon(
                                        Icons.watch_later,
                                        color: Colors.yellow,
                                      ),
                              ),
                            );
                          }))
                ],
              ),
            )),
      ),
    );
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
