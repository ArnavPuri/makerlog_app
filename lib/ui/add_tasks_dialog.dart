import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:get_makerlog/models/task_create.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:convert';
import 'package:path/path.dart';

class AddEntryDialog extends StatefulWidget {
  @override
  AddEntryDialogState createState() => new AddEntryDialogState();
}

final Widget emptyWidget = new Container(width: 0, height: 0);

class AddEntryDialogState extends State<AddEntryDialog> {
  TextEditingController _textController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  File currentImageFile;
  List<TaskCreate> _addedTasks = [];
  String newTask = "";
  bool isLoading = false;
  var taskCreateUrl = "https://api.getmakerlog.com/tasks/";

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 8.0,
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
          title: Text("Add tasks"),
          actions: [
            new FlatButton(
                onPressed: () {
                  if (_addedTasks.length > 0) {
                    _createTasks().then((status) {
                      if (status) {
                        Navigator.of(context).pop();
                      } else {
                        _scaffoldKey.currentState.showSnackBar(
                            SnackBar(content: Text("Something went wrong ")));
                      }
                    });
                  }
                },
                child: new Text('SAVE',
                    style: Theme.of(context)
                        .textTheme
                        .subhead
                        .copyWith(color: Colors.white))),
          ],
        ),
        body: new Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            isLoading
                ? Container(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.greenAccent,
                    ),
                    decoration: BoxDecoration(color: Colors.white),
                    alignment: Alignment.topRight,
                    padding: EdgeInsets.all(20.0),
                  )
                : emptyWidget,
            new Image.asset(
              "assets/images/empty_task_image.png",
              fit: BoxFit.contain,
              width: 250.0,
              alignment: Alignment.bottomCenter,
            ),
            new Column(children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: new ListTile(
                  trailing: currentImageFile == null
                      ? FlatButton.icon(
                          onPressed: () {
                            imageSelectorGallery().then((data) {
                              setState(() {});
                            });
                          },
                          label: Text("Attach"),
                          icon: Icon(Icons.image),
                        )
                      : new FadeInImage(
                          fit: BoxFit.fitHeight,
                          height: 100.0,
                          placeholder:
                              new AssetImage("assets/images/placeholder.png"),
                          image: FileImage(currentImageFile),
                        ),
                  title: TextField(
                    controller: _textController,
                    maxLines: 3,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(), hintText: "Add Task"),
                  ),
                ),
              ),
              FlatButton.icon(
                onPressed: () {
                  addTaskToList().then((data) {
                    setState(() {});
                  });
                },
                label: Text("Add Task"),
                color: Colors.greenAccent,
                icon: Icon(Icons.add),
              ),
              new Expanded(
                child: new ListView.builder(
                    itemCount: _addedTasks.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        child: _buildTaskCard(_addedTasks, index),
                      );
                    }),
              )
            ]),
          ],
        ));
  }

  Future imageSelectorGallery() async {
    var imagePicked = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );

    setState(() {
      if (imagePicked != null)
        currentImageFile = imagePicked;
      else
        currentImageFile = null;
    });
  }

  Future addTaskToList() async {
    newTask = _textController.text;
    if (newTask.isNotEmpty) {
      setState(() {
        _addedTasks.add(new TaskCreate(currentImageFile, newTask));
        _textController.text = "";
        currentImageFile = null;
      });
    }
  }

  Future _removeTask(item) async {
    if (_addedTasks.contains(item)) {
      setState(() {
        _addedTasks.remove(item);
      });
    }
  }

  Widget _buildTaskCard(data, index) {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))),
      child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: ListTile(
            contentPadding:
                EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            leading: data[index].imageFile != null
                ? Image.file(
                    data[index].imageFile,
                    height: 50,
                    fit: BoxFit.fitHeight,
                  )
                : emptyWidget,
            trailing: new FloatingActionButton(
              backgroundColor: Color.fromRGBO(64, 75, 96, .8),
              elevation: 0,
              mini: true,
              heroTag: hashCode,
              child: Icon(Icons.close),
              onPressed: () {
                _removeTask(data[index]);
              },
            ),
            title: new Text(data[index].task.toString(),
                style: TextStyle(
                  color: Colors.white,
                )),
          )),
    );
  }

  Future<bool> _createTasks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    var response = false;
    setState(() {
      isLoading = true;
    });
    for (var i = 0; i < _addedTasks.length; i++) {
      String taskContent = _addedTasks[i].task;
      File imageFile = _addedTasks[i].imageFile;

      if (imageFile != null) {
        var stream =
            new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
        var length = await imageFile.length();
        var uri = Uri.parse(taskCreateUrl);
        // create multipart request
        var request = new http.MultipartRequest("POST", uri);
        var multipartFile = new http.MultipartFile('attachment', stream, length,
            filename: basename(imageFile.path));
        request.files.add(multipartFile);
        request.headers["Authorization"] = "Token " + token;
        request.fields["content"] = taskContent;
        // send
        response = await request.send().then((value) {
          value.stream.transform(utf8.decoder).listen((value) {
            print(value.toString());
          });
          if (value.statusCode == 201) {
            return true;
          } else {
            return false;
          }
        });
      } else {
        response = await http.post(taskCreateUrl, headers: {
          HttpHeaders.authorizationHeader: "Token " + token
        }, body: {
          "content": taskContent,
        }).then((res) {
          print(res.body);
          if (res.statusCode == 201) {
            return true;
          } else {
            return false;
          }
        });
      }
    }
    setState(() {
      isLoading = false;
    });
    return response;
  }
}
