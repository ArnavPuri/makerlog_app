import 'dart:core';
import 'package:flutter/material.dart';
import 'bottom_app_bar.dart';
import 'package:get_makerlog/ui/tasks_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:convert';
import 'package:async/async.dart';
import 'package:path/path.dart';
import 'package:get_makerlog/models/ml_user.dart';
import 'package:get_makerlog/main.dart';
import 'package:get_makerlog/models/task.dart';
import 'package:get_makerlog/ui/add_tasks_dialog.dart';
import 'package:get_makerlog/ui/notifications_page.dart';
import 'package:get_makerlog/ui/SwipeAnimation/index.dart';

class MainPage extends StatefulWidget {
  const MainPage();

  @override
  _MainPageState createState() => _MainPageState();
}

final Widget emptyWidget = new Container(width: 0, height: 0);

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  TextEditingController taskEditController;
  var newTask = "";
  var taskCreateUrl = "https://api.getmakerlog.com/tasks/";
  var userDetailUrl = "https://api.getmakerlog.com/me/";
  var notificationsCheckUrl =
      "https://api.getmakerlog.com/notifications/unread_count/";

  int _selectedTab = 0;
  File imageFile;
  User currentUser;
  var appBarTitleText = new Text("Makerlog");
  var appBarStreakText = new Text("");
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  //notification vars
  bool hasUnreadNotifications = false;
  @override
  void initState() {
    taskEditController = new TextEditingController();
    super.initState();
    print("hey");
    _getUserStreak().then((value) {
      currentUser = value;
      setState(() {
        appBarTitleText = Text("Hi, ${currentUser.firstName}");
        appBarStreakText = Text(
            "🏁${currentUser.tda.toString()} 🔥️${currentUser.streak.toString()}");
      });
    });
    _tabController = TabController(length: 3, vsync: this);
    checkNotifications().then((status) {
      print(status);
      if (status) {
        hasUnreadNotifications = true;
      } else {
        hasUnreadNotifications = false;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  Widget _buildTabContent() {
    return Positioned.fill(
      child: TabBarView(
        controller: _tabController,
        physics: const NeverScrollableScrollPhysics(),
        children: [
          TasksPage(),
          NotificationPage(),
          CardDemo()
//          const ExplorePage(),
        ],
      ),
    );
  }

  void _tabSelected(int newIndex) {
    setState(() {
      _selectedTab = newIndex;
      _tabController.index = newIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    var content = Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      appBar: AppBar(
        elevation: 8.0,
        automaticallyImplyLeading: false,
        backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
        title: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[appBarTitleText, appBarStreakText],
        ),
      ),
//      bottomNavigationBar: BottomAppBar(),
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.done,
                color: _selectedTab == 0 ? Colors.greenAccent : Colors.black,
              ),
              tooltip: "Tasks",
              onPressed: () {
                setState(() {
                  _tabSelected(0);
                });
              },
            ),
            IconButton(
              icon: Icon(hasUnreadNotifications
                  ? Icons.notifications_active
                  : Icons.notifications_none),
              tooltip: "Notifications",
              color: _selectedTab == 1 ? Colors.greenAccent : Colors.black,
              onPressed: () {
                setState(() {
                  _tabSelected(1);
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.arrow_drop_up),
              tooltip: ".",
              color: Colors.white,
              onPressed: () {},
            ),
//            new Stack(children: <Widget>[
//              new InkWell(
//                child: Icon(Icons.notifications_none),
//                onTap: () {
//                  setState(() {
//                    _tabSelected(1);
//                  });
//                },
//              ),
//              new Positioned(
//                  top: -0.5,
//                  right: -0.5,
//                  child: new Stack(
//                    children: <Widget>[
//                      new Icon(
//                        Icons.brightness_1,
//                        size: 12.0,
//                        color: Colors.greenAccent,
//                      ),
//                    ],
//                  ))
//            ]),
            IconButton(
              icon: Icon(Icons.explore),
              tooltip: "Explore",
              onPressed: () {
                setState(() {
                  _tabSelected(2);
                });
//                _scaffoldKey.currentState.showSnackBar(new SnackBar(
//                  content: Text("Coming soon 😛"),
//                  duration: Duration(seconds: 1),
//                ));
              },
            ),
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {
                _showLogoutConfirmDialog(context);
              },
              tooltip: "Logout",
            ),
          ],
        ),
        shape: CircularNotchedRectangle(),
      ),
      //Traditional navbar
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.add,
          color: Colors.black,
        ),
        onPressed: () {
          _showAddTaskDialog(context);
        },
        backgroundColor: Colors.greenAccent,
      ),

      body: Stack(
        children: [
          _buildTabContent(),
        ],
      ),
    );

    return Stack(
      fit: StackFit.expand,
      children: [
        content,
      ],
    );
  }

  void _showLogoutConfirmDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              title: Text("Logout"),
              content: Text("Are you sure you want to logout?"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  child: new Text("Close"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                new FlatButton(
                  child: new Text("Yes"),
                  onPressed: () {
                    _logout(context);
                  },
                ),
              ]);
        });
  }

  void _showAddTaskDialog(BuildContext context) async {
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        // return object of type Dialog
//        return AlertDialog(
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.all(Radius.circular(8.0))),
//          title: new Text("Add new task"),
//          content: new Column(
//            mainAxisSize: MainAxisSize.min,
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//              new Container(
//                  child: new TextField(
//                decoration: new InputDecoration(
//                    hintText: "Seize the day ⚡️", border: OutlineInputBorder()),
//                keyboardType: TextInputType.multiline,
//                maxLines: 3,
//                controller: taskEditController,
//              )),
//              FlatButton.icon(
//                onPressed: () {
//                  imageSelectorGallery().then((data) {
//                    setState(() {});
//                  });
//                },
//                label: Text("Attach"),
//                icon: Icon(Icons.image),
//              ),
//              imageFile == null
//                  ? emptyWidget
//                  : new FadeInImage(
//                      fit: BoxFit.contain,
//                      width: 100.0,
//                      placeholder:
//                          new AssetImage("assets/images/placeholder.png"),
//                      image: FileImage(imageFile),
//                    )
//            ],
//          ),
//          actions: <Widget>[
//            // usually buttons at the bottom of the dialog
//            new FlatButton(
//              child: new Text("Close"),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//            new FlatButton(
//              child: new Text("Save"),
//              highlightColor: Colors.black,
//              onPressed: () {
//                setState(() {
//                  newTask = taskEditController.text;
//                  _createTask().then((status) {
//                    print(status);
//                    if (status) {
//                      Navigator.of(context).pop();
//                      imageFile = null;
//                      taskEditController.text = "";
//                    } else {
//                      print("error");
//                    }
//                    setState(() {});
//                  });
//                });
////                Navigator.of(context).pop();
//              },
//            ),
//            new CircularProgressIndicator()
//          ],
//        );
//      },
//    );
    List<Task> tasks =
        await Navigator.of(context).push(new MaterialPageRoute<Null>(
            builder: (BuildContext context) {
              return new AddEntryDialog();
            },
            fullscreenDialog: true));
    if (tasks != null) {
      print(tasks);
    }
  }

  Future imageSelectorGallery() async {
    var imagePicked = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (imagePicked != null)
        imageFile = imagePicked;
      else
        imageFile = null;
    });
  }

  Future<bool> checkNotifications() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print(token);
    var response = http.get(notificationsCheckUrl, headers: {
      HttpHeaders.authorizationHeader: "Token " + token
    }).then((data) {
      print(data.body);
      var count = json.decode(data.body)["unread_count"];
      if (count != null && count > 0) {
        return true;
      } else
        return false;
    });

    return response;
  }

  Future<User> _getUserStreak() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    var response = http.get(userDetailUrl, headers: {
      HttpHeaders.authorizationHeader: "Token " + token
    }).then((data) {
      print(data.body);
      return User.fromJson(json.decode(data.body));
    });
    setState(() {});
    return response;
  }

  void _logout(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear().then((done) {
      if (done) {
        Navigator.pop(context);
        imageCache.clear();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen()),
        );
      }
    });
  }
}
