import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:get_makerlog/models/web_notification.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'dart:async';
import 'package:intl/intl.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage();
  @override
  _MainFetchDataState createState() => _MainFetchDataState();
}

final Widget emptyWidget = new Container(width: 0, height: 0);

class _MainFetchDataState extends State<NotificationPage>
    with AutomaticKeepAliveClientMixin<NotificationPage> {
  List<WebNotification> list = List();
  Future<List<WebNotification>> notificationsList;
  List notifications;
  List jsonList = List();
//  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
//      new GlobalKey<RefreshIndicatorState>();
  var isLoading = true;
  var token = "";

  var notificationsURL = "https://api.getmakerlog.com/notifications/";
  var readNotificationsURL =
      "https://api.getmakerlog.com/notifications/mark_all_read/";

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<List<WebNotification>> getNotifications() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print(token);
    final response = await http.get(
      notificationsURL,
      headers: {HttpHeaders.authorizationHeader: "Token " + token},
    ).then((response) {
      print(response);
      print(response.body);
      var list = (json.decode(utf8.decode(response.bodyBytes)) as List)
          .map((content) => new WebNotification.fromJson(content))
          .toList();

      return list;
    });
    return response;
  }

  Future<bool> markAllRead() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await http.get(
      readNotificationsURL,
      headers: {HttpHeaders.authorizationHeader: "Token " + token},
    ).then((response) {
      var status = json.decode(response.body)["success"];
      return status;
    });
    return response;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: true,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      body: new FutureBuilder<List<WebNotification>>(
          future: notificationsList,
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              notifications = snapshot.data;
              return ListView(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                            child: Text(
                              "Notifications",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20.0, color: Colors.greenAccent),
                            ),
                            margin: EdgeInsets.only(right: 12.0)),
                        notifications.length != 0
                            ? Chip(
                                label: Text(notifications.length.toString()),
                              )
                            : emptyWidget,
                        Align(
                          child: IconButton(
                              icon: Icon(
                                Icons.clear_all,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                markAllRead().then((status) {
                                  if (status) {
                                    _scaffoldKey.currentState.showSnackBar(
                                        new SnackBar(
                                            content: Text("All caught up 👍")));
                                    setState(() {
                                      notificationsList = getNotifications();
                                    });
                                  }
                                });
                              }),
                          alignment: AlignmentDirectional.centerEnd,
                        )
                      ],
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                    margin: EdgeInsets.only(top: 18.0),
                  ),
                  notifications.isEmpty
                      ? Center(
                          child: Padding(
                          padding: EdgeInsets.all(24.0),
                          child: Text(
                            "No new notifications 😬",
                            style:
                                TextStyle(fontSize: 24.0, color: Colors.white),
                          ),
                        ))
                      : new ListView.builder(
                          itemCount: notifications.length,
                          physics: ClampingScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            return _buildNotificationCard(notifications, index);
                          }),
                ],
              );
            } else {
              return new Center(child: new CircularProgressIndicator());
            }
          }),
    );
  }

//  Future<Null> _refresh() {
//    return getTasks().then((list) {
//      setState(() {});
//    });
//  }

  Widget _buildNotificationCard(data, index) {
    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: ListTile(
            contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right:
                          new BorderSide(width: 1.0, color: Colors.white24))),
              child: Icon(
                data[index].key == "received_praise"
                    ? Icons.favorite
                    : Icons.comment,
                color: data[index].read ? Colors.white30 : Colors.white,
              ),
            ),
            title: new Text("${data[index].actor} ${data[index].verb}",
                style: data[index].read
                    ? TextStyle(
                        color: Colors.white30,
//                                                  fontFamily: "Poppins",
                      )
                    : TextStyle(
                        color: Colors.white,
//                                                  fontFamily: "Poppins",
                      )),
            subtitle: data[index].target != null
                ? new Text(
                    "${data[index].target}",
                    style: new TextStyle(color: Colors.white30),
                    maxLines: 2,
                  )
                : new Text(""),
          )),
    );
  }

  @override
  void initState() {
    super.initState();
    notificationsList = getNotifications();
  }

  @override
  bool get wantKeepAlive => true;
}
