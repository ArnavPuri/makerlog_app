import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get_makerlog/ui/main_page.dart';
import 'package:get_makerlog/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:core';

var flutterLocalNotificationsPlugin;

// This is the Splash Screen
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _animationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 1500));
    _animation = new CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeOut,
    );

    _animation.addListener(() => this.setState(() {}));
    _animationController.forward();

    Timer(Duration(seconds: 2), () {
      getLogin();
    });
    //  notification settings
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                color: Colors.black,
                image: DecorationImage(
                  colorFilter: new ColorFilter.mode(
                      Colors.black.withOpacity(0.2), BlendMode.dstATop),
                  image: AssetImage('assets/images/mountains.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              child: new Text("")),
          Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/logo.png',
                      width: _animation.value * 100.0,
                      height: _animation.value * 100.0,
                      fit: BoxFit.fitWidth,
                    ),
                    Padding(padding: EdgeInsets.only(top: 10.0)),
                    Text(
                      "Makerlog",
                      style: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ])
        ],
      ),
    );
  }

  void getLogin() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? "";
    if (token == "") {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginScreen()));

      //setState to refresh or move to some other page
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MainPage()));
    }
  }
}
