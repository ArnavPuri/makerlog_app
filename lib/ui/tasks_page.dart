import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:get_makerlog/models/task.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'dart:async';
import 'package:intl/intl.dart';

class TasksPage extends StatefulWidget {
  const TasksPage();
  @override
  _MainFetchDataState createState() => _MainFetchDataState();
}

final Widget emptyWidget = new Container(width: 0, height: 0);

class _MainFetchDataState extends State<TasksPage>
    with AutomaticKeepAliveClientMixin<TasksPage> {
  List<Task> list = List();
  Future<List<Task>> tasksList;
  List pendingTasks;
  List completedTasks;
  List jsonList = List();
//  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
//      new GlobalKey<RefreshIndicatorState>();
  var isLoading = true;
  var token = "";

  var tasksURL = "https://api.getmakerlog.com/tasks/sync/";
  var taskDoneURL = "https://api.getmakerlog.com/tasks/";

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<List<Task>> getTasks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    final response = await http.get(
      tasksURL,
      headers: {HttpHeaders.authorizationHeader: "Token " + token},
    ).then((response) {
      var list = (json.decode(utf8.decode(response.bodyBytes))["data"] as List)
          .map((content) => new Task.fromJson(content))
          .toList()
          .reversed
          .toList();

      return list;
    });
    return response;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: true,
      backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
      body: new FutureBuilder<List<Task>>(
          future: getTasks(),
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              pendingTasks = snapshot.data.where((i) => !i.done).toList();
              completedTasks = snapshot.data.where((i) => i.done).toList();
              return ListView(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                            child: Text(
                              "Pending Tasks",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20.0, color: Colors.greenAccent),
                            ),
                            margin: EdgeInsets.only(right: 12.0)),
                        pendingTasks.length != 0
                            ? Chip(
                                label: Text(pendingTasks.length.toString()),
                              )
                            : emptyWidget
                      ],
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                    margin: EdgeInsets.only(top: 18.0),
                  ),
                  pendingTasks.isEmpty
                      ? Center(
                          child: Padding(
                          padding: EdgeInsets.all(24.0),
                          child: Text(
                            "All caught up! 👏",
                            style:
                                TextStyle(fontSize: 32.0, color: Colors.white),
                          ),
                        ))
                      : new ListView.builder(
                          itemCount: pendingTasks.length,
                          physics: ClampingScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (BuildContext context, int index) {
                            final item = pendingTasks[index];

                            return Dismissible(
                                key: UniqueKey(),
                                onDismissed: (direction) async {
                                  var response = await _markTaskAsDone(
                                      pendingTasks[index].id.toString());
                                  if (response) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text("Well done 👏")));
                                  } else {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text("Error Occured 😣")));
                                  }

                                  setState(() {
                                    pendingTasks.removeAt(index);
                                    tasksList.then((list) {
                                      list.remove(item);
                                    });
                                  });
                                },
                                background: Container(
                                  color: Colors.greenAccent,
                                  child: const Icon(Icons.done),
                                ),
                                child: _buildTaskCard(pendingTasks, index));
                          }),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                            child: Text(
                              "Done & Dusted",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20.0, color: Colors.greenAccent),
                            ),
                            margin: EdgeInsets.only(right: 12.0)),
                        completedTasks.length != 0
                            ? Chip(
                                label: Text(completedTasks.length.toString()),
                              )
                            : emptyWidget
                      ],
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                    margin: EdgeInsets.only(top: 12.0, bottom: 12.0),
                  ),
                  new ListView.builder(
                      itemCount: completedTasks.length,
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildTaskCard(completedTasks, index);
                      })
                ],
              );
            } else {
              return new Center(child: new CircularProgressIndicator());
            }
          }),
    );
  }

  Future<bool> _markTaskAsDone(id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    print("Task done called:");
    var response = await http.patch(taskDoneURL + id + '/', headers: {
      HttpHeaders.authorizationHeader: "Token " + token
    }, body: {
      "done": "true",
    });
    print(response.statusCode);
    print(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

//  Future<Null> _refresh() {
//    return getTasks().then((list) {
//      setState(() {});
//    });
//  }

  Widget _buildTaskCard(data, index) {
    return Card(
      elevation: 4.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: ListTile(
            contentPadding:
                EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right:
                          new BorderSide(width: 1.0, color: Colors.white24))),
              child: !data[index].done
                  ? Icon(Icons.priority_high, color: Colors.white)
                  : Icon(Icons.done),
            ),
            trailing: data[index].attachment != null
                ? Icon(Icons.attach_file)
                : emptyWidget,
            title: new Text(data[index].task.toString(),
                style: !data[index].done
                    ? TextStyle(
                        color: Colors.white,
//                                                  fontFamily: "Poppins",
                      )
                    : TextStyle(
                        color: Colors.white30,
                        fontFamily: "Poppins",
                      )),
            onTap: () {
              showModalBottomSheet<void>(
                  context: context,
                  builder: (BuildContext context) {
                    return Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          mainAxisSize: data[index].attachment != null
                              ? MainAxisSize.max
                              : MainAxisSize.min,
                          children: <Widget>[
                            Text(data[index].task,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 18.0)),
                            new Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text("Added on: " +
                                    DateFormat('dd-MM-yyyy').format(
                                        DateTime.parse(data[index].dateAdded))),
                                Text(data[index].done
                                    ? "Status: Done"
                                    : "Status: Pending"),
                              ],
                            ),
                            data[index].attachment != null
                                ? Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: new FadeInImage(
                                        height: 220,
                                        fit: BoxFit.fitHeight,
                                        placeholder: new AssetImage(
                                            "assets/images/placeholder.png"),
                                        image: NetworkImage(
                                          data[index].attachment,
                                        )),
                                  )
                                : new Text(""),
                            !data[index].done
                                ? RaisedButton(
                                    onPressed: () async {
                                      var response = await _markTaskAsDone(
                                          pendingTasks[index].id.toString());
                                      Navigator.pop(context);
                                      if (response) {
                                        _scaffoldKey.currentState.showSnackBar(
                                            SnackBar(
                                                content: Text("Well done 👏")));

                                        setState(() {
                                          tasksList.then((list) {
                                            list.remove(data[index]);
                                          });
                                        });
                                      } else {
                                        _scaffoldKey.currentState.showSnackBar(
                                            SnackBar(
                                                content:
                                                    Text("Error Occured 😣")));
                                      }
                                    },
                                    color: Colors.greenAccent,
                                    child: Text("Mark as done"),
                                  )
                                : Text("Completed on: " +
                                    DateFormat('dd-MM-yyyy').format(
                                        DateTime.parse(
                                            data[index].dateCompleted))),
                          ],
                        ));
                  });
            },
          )),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    tasksList = getTasks();
  }

  @override
  void initState() {
    super.initState();
    tasksList = getTasks();
  }

  @override
  bool get wantKeepAlive => true;
}
